import net.schmizz.sshj.SSHClient;
import net.schmizz.sshj.connection.channel.direct.Session;
import java.io.IOException;

public class Connection {

    private String ipAddress = "192.168.222.110";
    private String username = "datreon";
    private String password = "Qwerty@321";

    protected Session session;
    protected SSHClient sshClient;

    public void makeConnection() throws IOException {
        sshClient = new SSHClient();
        sshClient.loadKnownHosts();

        sshClient.connect(ipAddress);
        sshClient.authPassword(username, password);

        session = sshClient.startSession();

        if(session != null){
            System.out.println("You are connected to remote system");
        }
        else
            System.out.println("Connection not done");

    }
}




