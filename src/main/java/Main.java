import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {
        ExecuteCmd e = new ExecuteCmd();
        e.makeConnection();
        e.execute();
    }
}
