import net.schmizz.sshj.common.IOUtils;
import net.schmizz.sshj.connection.channel.direct.Session;
import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class ExecuteCmd extends Connection {

    public void execute() throws IOException {
        Session.Command cmd = session.exec("ls");

        System.out.println(IOUtils.readFully(cmd.getInputStream()).toString());
        cmd.join(1, TimeUnit.SECONDS);

        session.close();

        sshClient.disconnect();

    }
}
